<?php

/**
 * Load scripts and styles
 *
 * @link        http://developer.wordpress.org/reference/functions/wp_enqueue_script
 * @link        http://wp-kama.ru/function/wp_enqueue_script
 *
 * @package     WordPress
 * @subpackage  RSV v3
 * @since       1.0.0
 * @author      Luke Kortunov
 */
function rst_load_assets() {
    //--- Load scripts and styles only for frontend: -----------------------------
    if( !is_admin() ) {
        wp_deregister_script( 'jquery' );
        // Styles
        wp_enqueue_style( 'app', get_template_directory_uri() . '/dist/app.min.css' );
        // Scripts
        wp_enqueue_script( 'app', get_template_directory_uri() . '/dist/app.min.js', [], '1.0.0', true );
    }
}
add_action( 'wp_enqueue_scripts', 'rst_load_assets' );
