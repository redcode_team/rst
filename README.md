# RED STARTER THEME
### Quick start with WordPress theme

## Intro
Modern, highly customizable and extendable WebPack based skeleton for WordPress themes, that support lot of nice features, like OOP-way, ES6, SCSS, hot-reload, linting, CI/CD templates for Bitbucket, GitLab, CircleCI.\
We love clean code, OOP, Modules, REST and pretty code, so please, [RTFM](https://redcode.com.ua/rst/v3/quick-start) =)

## Stack
- PHP 7.0
- MySQL 5.7
- HTML5
- SCSS
- ES6 (Modern Javascript)
- Webpack
- NodeJS (npm & yarn)
- PHP Composer
- ESLint, stylelint-scss
