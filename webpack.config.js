'use strict';

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {

    mode: 'development',

    entry: {
        app: './src/js/app.js',
    },

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].min.js'
    },

    watch: true,

    watchOptions: {
        aggregateTimeout: 100
    },

    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'eslint-loader',
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader']
                })
            }
        ]
    },

    optimization: {
        runtimeChunk: {
            name: 'manifest'
        }
    },

    plugins: [
        new ExtractTextPlugin({
            filename: 'app.min.css'
        })
    ],

    stats: {
        colors: true,
        children: false,
    },

    devtool: 'cheap-inline-module-source-map'

};
