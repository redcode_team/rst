'use strict';

module.exports = function (numbers) {

    return numbers.filter(element => {
        return element > 0;
    });

};
