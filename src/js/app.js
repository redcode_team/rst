'use strict';

/**
 * Import styles to save single entry point in webpack config
 */
import '../scss/app.scss';


/**
 * Import demo ES6 module
 */
import Filter from './modules/Filter';


/**
 * Example usage
 * @type {number[]}
 */
let numbers = [1,-1,-25,17];
console.log( Filter(numbers) );
